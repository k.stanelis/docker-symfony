docker-symfony
==============

This is a complete stack for running Symfony (latest version: Flex) into Docker containers using docker-compose tool.

### THIS IS FOR DEVELOPMENT ENVIRONMENT ONLY! THIS IS NOT SUITED FOR PRODUCTION ENVIRONMENT!

# Installation

First, clone this repository:

```bash
$ git clone git@gitlab.com:k.stanelis/docker-symfony.git
```

Symfony project should go to `src` folder. To define custom address edit `server_name` in `nginx/localhost.conf` file.

Then, run:

```bash
$ docker-compose up
```

You are done, you can visit your Symfony application on the following URL: `http://localhost`

By default all Kibana features are commented out in `docker-compose.yml` file. If you want to use them just uncomment them. To access Kibana go to `http://localhost:81` (Assuming you did not change `server_name`) 

Node container is used for webpack usage. 

# Read logs

You can access Nginx and Symfony application logs in the following directories on your host machine:

* `logs/nginx`
* `logs/symfony`

# Use xdebug!

Configure your IDE to use port 5902 for XDebug.
Docker versions below 18.03.1 don't support the Docker variable `host.docker.internal`.  
In that case you'd have to swap out `host.docker.internal` with your machine IP address in php-fpm/xdebug.ini.
For CLI debuging for DB connection use docker local IP. You can get it from phpinfo() `$_SERVER['REMOTE_ADDR']`.

# Code license

You are free to use the code in this repository under the terms of the 0-clause BSD license. LICENSE contains a copy of this license.
